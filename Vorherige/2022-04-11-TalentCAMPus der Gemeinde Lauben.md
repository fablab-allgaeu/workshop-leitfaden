# TalentCAMPus der Gemeinde Lauben

## Eckdaten
| Titel          | TalentCAMPus der Gemeinden Lauben, Sulzberg und Dietmannsried |
| -------------- | ------------------------------------------------------------- |
| Dauer          | 11.4.2022 - 14.4.2022 je 9-16 Uhr |
| Veranstalter   | Jugendpfleger Sulzberg |
| Ort            | Werkräume der Grundschule Lauben |
| Teilnehmer     | Kinder und Jugendliche zwischen 10 und 15 Jahren |
| Teilnehmerzahl | 16 |

**Sonstiges:**
  - Verpflegung zur Mittagspause im Jugendhaus (Essen wurde geliefert)
  - keine Getränke vor Ort
  - parallel zum Angebot des FabLab wurde noch Lederverarbeitung angeboten
  - die Teilnehmer wurden in zwei Gruppen aufgeteilt, die nach der Mittagspause die Tätigkeit gewechselt haben
  - Betreuer wurden entlohnt
  - zusätzliche Aufwände wie Material und Fahrtkosten wurden auch erstattet

## Kontakt zu den Veranstaltern
Der Jugendpfleger aus Sulzberg hat unsere Webseite in Google gefunden und uns anschließend per Mail kontaktiert.

## Organisation
Die Organisation unsererseits erfolgte großteils über Telegram. Das erste vorbereitende Treffen fand am Donnerstag vor dem Workshop statt.
**Timeline:**   
  -  8. Dez 2021: Anfrage vom Jugendpfleger Sulzberg
  - 11. Dez 2021: Telegram Gruppe zur Organisation eröffnet, Ideen von @_segfault vorgestellt.
  - 17. Dez 2021: "sehr wahrscheinliche" Zusage an den Jugendpfleger
  -  2. Feb 2022: Gedanken über den Materialpreis
  -  9. Mär 2022: konkretere Infos über die Ausstattung am Veranstaltungsort
  - 28. Mär 2022: Realisation, dass zu wenig Material vorhanden ist (Entscheidungsfinder)
  - 31. Mär 2022: virtuelles Treffen mit den Veranstaltern, genauere Infos auch zu benötigten Dokumenten (erweitertes Führungszeugnis)
  -  5. Apr 2022: Panikbestellung der fehlenden Materialien
  -  7. Apr 2022: Treffen zur genaueren Organisation, Einteilung der Tage, "Hat eigentlich jemand Vorlagen für Lötübungen?"
  -  9. Apr 2022: Reichelt liefert wie gewohnt just in time
  -  9. Apr 2022: Panikkäufe der sonstigen Materialien (Draht)
  - 10. Apr 2022: Schnelle Suche nach Vorlagen für Lötübungen
  - 11. Apr 2022: Aufbaubeginn 8:30 nach Verzögerung durch Umleitung

## Ablauf des Workshops
### Montag, Löten der Skulptur "Eifelturm", Betreuer: @_segfault und @koefmatt
Vorab mussten wir die Arbeitsplätze einrichten, d.h. Strom an die Tische legen oder die Tische zum Strom bringen. Zu Beginn war es etwas chaotisch, da wir nicht
geplant hatten, wie die Kinder anfangen sollten. Letztendlich mussten wir erst einige Meter Draht abisolieren, bevor sie beginnen konnten.
Das hat viel Aufmerksamkeit gekostet. Wir hatten die Lötübungen selbst auch noch nicht ausprobiert, wussten also nicht wie schwer sie sind.
Nach den Startschwierigkeiten konnten die Kinder dann aber anfangen, ihre Drähte nach den Vorlagen abzulängen und zu biegen. Danach war das Problem, dass alle
ziemlich gleichzeitig fertig wurden und auf uns warten mussten, da die erste Lötstelle auch gleich die schwierigste war (die Spitze mit 4 Drähten zusammenlöten).
Wir haben den Zeitaufwand auch stark unterschätzt. Viele wurden am Vormittag gar nicht fertig. Die Nachmittagsgruppe war deutlich schneller, was aber wahrscheinlich
auch daran lag, dass wir nun wussten, wie man die Vorlage aufbaut. Die Kinder hatten bei der Gestaltung des Turms die Freiheit, welches Muster sie für die Seiten
verwenden wollten und waren dabei auch sehr kreativ.   

**Positiv:**
  - für die allermeisten der erste Kontakt mit dem Lötkolben (nur eine Teilnehmerin hat bereits Elektronik gelötet, einige andere nur mit Holzbrennpinseln gearbeitet)
  - die Kinder waren sehr interessiert
  - jeder hat den Eifelturm fertig gestellt
  - die Aufgaben waren abwechslungsreich genug, dass die Kinder bei der Sache blieben (ablängen, biegen, löten, gestalten)

**Probleme:**
  - wir wussten selbst nicht, wie man die Vorlagen aufbaut
  - durch die komplexe erste Lötstelle mussten alle Kinder auf uns warten
  - wir wussten nicht, wie viel Draht man braucht, obwohl das aus den Vorlagen ersichtlich wäre
  - die Kinder haben oft nicht auf die Lage der Lötkolben geachtet, was z.B. zu verschmorten Netzkabeln geführt hat
  - wir hatten erst keine Lötmatten und dann welche auch PVC, die auch verschmort sind
  - die Kinder haben sich über den Kolophoniumdampf beschwert
  - keine Möglichkeit, die Lötspitzen zu reinigen

### Dienstag, Programmieren mit Scratch, Betreuer: @_segfault und @koefmatt
Wir haben glücklicherweise genügend iPads vom Veranstaltungsort geliehen bekommen, sodass jeder Teilnehmer an seinem eigenen Gerät arbeiten konnte. Es waren sogar
genügend Geräte vorhanden, dass wir sie zur Mittagszeit gegen voll geladene iPads austauschen konnten. Jan hat die Schritte jeweils über einen Projektor gezeigt
und die Kinder konnten ihm nachmachen. Allerdings waren sie oft abgelenkt und haben nicht auf den gezeigten Code geachtet, was zu vielen Nachfragen geführt hat.
Generell benötigten die meisten viel intensive Betreuung, um voran zu kommen. Haben sie diese nicht zeitnah bekommen, wurden sie frustriert und haben sich mit
anderen Funktionen der App beschäftigt (z.B. Sprites malen oder Sounds bearbeiten, zum Leidwesen aller Anwesenden...). Das vorgegebene Beispiel für das Fangen
von Gegenständen haben alle hinbekommen. Ein weiteres Problem bestand in der Bedienung von Scratch per Touchdisplay, da dies oft ungenau und somit frustrierend
war. Des weiteren gab es viele kleine Probleme mit der Scratch Anwendung an sich, die auch für Betreuer bemerkbar waren (Teile des UI von der Tastatur versteckt,
keine physischen Tasten da keine Tastatur). Nachdem die Gruppen ihre Arbeit jeweils beendet hatten, mussten wir händisch alle Dateien herunterladen und speichern,
damit wir sie den Teilnehmern nachher zuschicken konnten. Insgesamt wurde dieser Teil des Workshops aber doch gut angenommen und viele haben nach dem Grundbeispiel
ihr Projekt mit eigenen Ideen erweitert.   

**Positiv:**
  - für viele der erste Kontakt mit der Programmierung
  - jeder Teilnehmer hatte sein eigenes Gerät
  - Teilnehmer konnten sich gegenseitig ihre Kreationen zeigen/spielen
  - auch für Teilnehmer mit geringem Interesse am Programmierteil gab es die Möglichkeit, Sounds und Sprites zu editieren

**Probleme:**
  - für einige war der Einstieg sehr schwer
  - andere hatten Scratch bereits in der Schule benutzt, weswegen der Workshop für die einen Schulcharakter bekommen hat oder nicht mehr spannend war
  - die Betreuung war sehr zeitintensiv
  - Teilnehmer, die vor einem Problem standen, wurden schnell frustriert
  - teilweise wurden die Scratch Projekte einfach zurückgesetzt, was zusätzlich Zeit zur Wiederherstellung gekostet hat
  - Touch ist für die Bedienung von Scratch eher nur bedingt geeignet
  - die Soundfunktion hat zwar allen Spaß gemacht, dafür aber auch alle anderen gestört
  - einige wussten nach der Grundfunktion nicht, was sie machen sollten

### Mittwoch, Aufbau des Bausatzes "Entscheidungsfinder", Betreuer: Bene F.
// TODO: hier bitte den Mittwoch eintragen

### Donnerstag, Freie Arbeit (Do What You Want), Betreuer: @koefmatt
Am letzten Tag gab es kein Rahmenprogramm. Die Teilnehmer waren frei in ihrer Wahl, was sie tun wollten. Nur vier Kinder haben sich für's Programmieren entschieden
und auch bald schon wieder ihre iPads weggelegt, um auch noch zu löten. Das scheint auch allen viel Spaß gemacht zu haben und es gab viele Ideen, wie Handyhalter
oder Osterschmuck. Nach zwei vorangegangenen Tagen mit Löterfahrung klappte das auch größtenteils eigenständig. Zwei Teilnehmer haben sich für ein weiteres
Modell aus den Vorlagen entschieden, diesmal den "Doppeldecker". Generell waren sie am letzten Tag sehr eigenständig.   

**Positiv:**
  - die Wahlmöglichkeit wurde von allen positiv aufgenommen
  - alle haben sofort mit eigenen Kreationen begonnen
  - die Teilnehmer haben sich mehr gegenseitig geholfen
  - es war eine sehr entspannte Atmosphäre

**Probleme:**
  - alle wollten die "coolen" Lötkolben
  - die Lötspitzen der TS100 sind ziemlich in Mitleidenschaft gezogen worden
  - die Kinder wurden unachtsam und haben sich oder ihre Kleidung öfter verbrannt
  - einige hätten sich mehr, dafür einfachere Vorlagen gewünscht
  - auch die Lötunterlagen waren am Ende ziemlich rampuniert und einige nicht mehr benutzbar
  - wir hatten nicht genug Netzteile für alle TS100 und auch keine Lötkolbenhalter

## Rückmeldung
### Von den Veranstaltern
Man hat sich von unserer Seite ein bisschen mehr Planung gewünscht, was den zeitlichen Ablauf anging. Es kam die Frage auf, ob wir die Vorlagen vorher überhaupt
ausprobiert hätten (upps...)

### Von den Teilnehmern
Das positivste Feedback bekamen wir zu den Entscheidungsfindern. Kein einziger hat einen "Daumen nach unten" gegeben. Allgemein wurden alle Angebote mehrheitlich
positiv wahrgenommen. Am meisten Kritik bekam das Programmieren in Scratch, wobei hier auch die Meinungen auseinander gingen. Positiv anzumerken ist, dass alle
die Verpflegung als das schlechteste in dieser Woche empfunden haben, also waren wir wohl gar nicht mal so schlecht. Kinder sind aber extrem brutal und direkt,
was Feedback angeht.

## Lessons Learned
  - Wir brauchen dringend hitzefeste Lötunterlagen
  - Wir sollten Netzteile und Halter für die TS100 besorgen
  - Wir brauchen ein interessanteres Konzept für Scratch oder ein anderes Programmierprojekt
  - Wir brauchen noch mehr kleine Lötprojekte mit Platinen. Möglichst einfach und günstig
  - Für 8 Kinder braucht man zwei Betreuer
  - Alle Betreuer müssen vorher alle Angebote ausprobieren und Probleme vorher zu finden
